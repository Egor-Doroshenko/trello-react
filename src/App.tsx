import React, { useState } from 'react';
import { css } from 'styled-components';
import Desc from './components/Desc/index';
import { PopupBg } from './ui/PopupBg';
import { Title } from './ui/Title';
import { Subtitle } from './ui/Subtitle';
import { Button } from './ui/Button';
import { Container } from './ui/Container';
import { Input } from './ui/Input';

const App: React.FunctionComponent = () => {
  let authorised = false;
  const user = localStorage.getItem('username');
  if (user) {
    authorised = true;
  }

  const [username, setUsername] = useState('');

  const changeHandler = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setUsername(event.target.value);
  };

  const keyPressHandler = (
    event: React.KeyboardEvent<HTMLInputElement>,
  ): void => {
    if (event.key === 'Enter') {
      localStorage.setItem('username', username);
      setUsername('');
    }
  };

  const clickHandler = (): void => {
    localStorage.setItem('username', username);
    setUsername('');
  };

  if (authorised === false) {
    return (
      <PopupBg show>
        <Container
          onClick={(e) => e.stopPropagation()}
          css={css`
            flex-direction: column;
            background: #C1FFD6;
            padding: 1rem;
            width: 50%;
          `}
        >
          <Title text="Not authorised!" />
          <Subtitle text="What's your name?" />
          <Input
            onChange={changeHandler}
            onKeyPress={keyPressHandler}
            value={username}
            css={css`width: 50%`}
          />
          <Button text="Lets go!" onClick={clickHandler} />
        </Container>
      </PopupBg>
    );
  }

  return (
    <Desc />
  );
};

export default App;
