export interface ICard {
  name: string,
  id: number,
  author: string
}

export interface IComment {
  author: string,
  text: string,
  id: number
}
