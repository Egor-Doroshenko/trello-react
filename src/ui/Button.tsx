import React from 'react';
import styled, { CSSProp } from 'styled-components/macro';

export const Button: React.FC<ButtonProps> = ({
  danger, onClick, text, borderRadiusSides, css,
} : ButtonProps) => (
  <ButtonStyled
    danger={danger}
    onClick={onClick}
    borderRadiusSides={borderRadiusSides}
    css={css}
  >
    {text}
  </ButtonStyled>
);

interface ButtonProps {
  danger?: boolean
  onClick: (event: React.MouseEvent) => void
  text?: string
  borderRadiusSides?: string
  css?: CSSProp
}
Button.defaultProps = {
  danger: false,
  text: '',
  borderRadiusSides: 'all',
  css: {},
};

const ButtonStyled = styled.button<ButtonProps>`
  padding: 0 1rem;

  ${(props) => (
    props.borderRadiusSides === 'right'
      ? 'border-top-right-radius: 10px; border-bottom-right-radius: 10px;' : ''
  )}
   ${(props) => (
    props.borderRadiusSides === 'all' ? 'border-radius: 10px;' : ''
  )}
  border: none;
  background: ${(props) => (props.danger ? '#ff9d9d' : '#57D782')};
  width: auto;
  height: auto;
  margin-bottom: 1rem;


  &:hover {
    background: ${(props) => (props.danger ? '#ef7d7d' : '#37b762')};
  }
`;
