import React from 'react';
import styled from 'styled-components';

export const Text: React.FC<TextProps> = (
  {
    size, bold, text, hidden,
  }: TextProps,
) => (
  <TextStyled
    size={size}
    bold={bold}
    hidden={hidden}
  >
    {text}
  </TextStyled>
);

interface TextProps {
  size?: string,
  bold?: boolean,
  text?: string
  hidden?: boolean
}
Text.defaultProps = {
  bold: false,
  size: '18px',
  text: '',
  hidden: false,
};

const TextStyled = styled.p<TextProps>`
  font-size: ${(props) => props.size};
  font-weight: ${(props) => (props.bold ? 'bold' : 'normal')};
  margin-bottom: 1rem;
  opacity: ${(props) => (props.hidden ? '0' : '1')};
  pointer-events: ${(props) => (props.hidden ? 'none' : 'all')};
  transition: 0.4s;
`;
