import React from 'react';
import styled, { CSSProp } from 'styled-components';

export const Input: React.FC<InputProps> = (
  {
    value,
    borderRadiusSides,
    onChange,
    onKeyPress,
    placeholder,
    hidden,
    css,
  }: InputProps,
) => (
  <InputStyled
    value={value}
    borderRadiusSides={borderRadiusSides}
    onChange={onChange}
    onKeyPress={onKeyPress}
    placeholder={placeholder}
    hidden={hidden}
    css={css}
  />
);

interface InputProps {
  value?: string
  borderRadiusSides?: string
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
  onKeyPress: (event: React.KeyboardEvent<HTMLInputElement>) => void
  placeholder?: string
  hidden?: boolean
  css?: CSSProp
}
Input.defaultProps = {
  value: '',
  borderRadiusSides: 'all',
  placeholder: 'Type here',
  hidden: false,
  css: {},
};

const InputStyled = styled.input<InputProps>`
  font-size: 18px;
  padding: 0.5rem;
  ${(props) => (
    props.borderRadiusSides === 'left'
      ? 'border-top-left-radius: 10px; border-bottom-left-radius: 10px;' : ''
  )}
   ${(props) => (
    props.borderRadiusSides === 'all' ? 'border-radius: 10px;' : ''
  )}
  border: none;
  width: 100%;
  height: 50px;
  margin-bottom: 0.5rem;

  opacity: ${(props) => (props.hidden === true ? '0' : '1')};
  width: ${(props) => (props.hidden === true ? '0' : props.width)};
  height: ${(props) => (props.hidden === true ? '0' : '50px')};
  pointer-events: ${(props) => (props.hidden === true ? 'none' : 'auto')};
`;
