import React, { useState, useEffect } from 'react';
import { css } from 'styled-components';
import { PopupBg } from '../../ui/PopupBg';
import { Subtitle } from '../../ui/Subtitle';
import { Title } from '../../ui/Title';
import { Text } from '../../ui/Text';
import { Comments } from '../Comments/index';
import { Button } from '../../ui/Button';
import { Container } from '../../ui/Container';
import { Input } from '../../ui/Input';
import { Clickable } from '../../ui/Clickable';

interface PopupProps {
  cardName: string,
  cardId: number,
  colName: string,
  setComm: React.Dispatch<React.SetStateAction<number>>
  author: string
}

export const Popup:
React.FunctionComponent<PopupProps> = (
  {
    cardName, cardId, colName, setComm, author,
  }: PopupProps,
) => {
  const [popupShow, setPopupShow] = useState(false);

  const [title, setTitle] = useState(cardName);

  const [descHidden, setDescHidden] = useState('');
  const [desc, setDesc] = useState(
    `${localStorage.getItem(`${title}_${cardId}_desc`)}`,
  );

  const [descEditHidden, setDescEditHidden] = useState(true);

  const description = localStorage.getItem(`${title}_${cardId}_desc`)!;

  const changeDesc = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setDescHidden(event.target.value);
  };

  const clickDesc = (): void => {
    if (descEditHidden === true) {
      setDescEditHidden(false);
      setDescHidden(description);
    } else {
      localStorage.setItem(`${cardName}_${cardId}_desc`, descHidden);
      setDescHidden('');
      setDesc(`${localStorage.getItem(`${cardName}_${cardId}_desc`)}`);
      setDescEditHidden(true);
    }
  };

  const keyPressDesc = (
    event: React.KeyboardEvent<HTMLInputElement>,
  ): void => {
    if (event.key === 'Enter') {
      localStorage.setItem(`${cardName}_${cardId}_desc`, descHidden);
      setDescHidden('');
      setDesc(`${localStorage.getItem(`${cardName}_${cardId}_desc`)}`);
      setDescEditHidden(true);
    }
  };

  const deleteDesc = (): void => {
    localStorage.setItem(
      `${cardName}_${cardId}_desc`, 'This card has no description yet!',
    );
    setDesc(`${localStorage.getItem(`${cardName}_${cardId}_desc`)}`);
  };

  const [editShow, setEditShow] = useState(false);
  const editClick = ():void => {
    setEditShow(true);
  };

  const [oldCardName, setOldCardName] = useState(cardName);

  const changeTitle = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setTitle(event.target.value);
  };

  const keyPressTitle = (event: React.KeyboardEvent): void => {
    if (event.key === 'Enter') {
      setEditShow(false);
      const newCol = localStorage.getItem(
        colName,
      )!.replace(oldCardName, title);
      localStorage.setItem(colName, newCol);

      const comments = localStorage.getItem(
        `${oldCardName}_${cardId}_comments`,
      )!;
      localStorage.setItem(`${title}_${cardId}_comments`, comments);

      const descript = localStorage.getItem(
        `${oldCardName}_${cardId}_desc`,
      )!;
      localStorage.setItem(`${title}_${cardId}_desc`, descript);

      localStorage.removeItem(`${oldCardName}_${cardId}_comments`);
      localStorage.removeItem(`${oldCardName}_${cardId}_desc`);

      setOldCardName(title);
    }
  };

  const escFunc = (event: KeyboardEvent): void => {
    if (event.keyCode === 27) {
      setPopupShow(false);
      setEditShow(false);
    }
  };

  useEffect(() => {
    document.addEventListener('keydown', escFunc);
  }, []);

  const cardData = `Created by ${author} in column "${colName}"`;

  return (
    <>
      <Clickable
        onClick={() => { setPopupShow(true); }}
      >
        <Title
          text={title}
        />
      </Clickable>
      <Button
        text="Edit"
        danger
        onClick={editClick}
      />

      <PopupBg
        show={editShow}
        onClick={() => { setEditShow(false); }}
      >
        <Container
          onClick={(e) => e.stopPropagation()}
          css={css`
              flex-direction: column;
              background: #C1FFD6;
              padding: 1rem;
              width: 50%;
          `}
        >
          <Container
            onClick={(e) => e.stopPropagation()}
            css={css`
              flex-direction: row;
              justify-content: space-between;
              align-items: flex-start;
              background: #C1FFD6;
              width: 100%;
          `}
          >
            <Subtitle
              text="Edit card name"
            />
            <span
              className="material-icons-outlined click"
              onClick={() => { setEditShow(false); }}
              onKeyPress={() => { setEditShow(false); }}
              role="button"
              tabIndex={0}
            >
              close
            </span>
          </Container>
          <Input
            value={title}
            onChange={changeTitle}
            onKeyPress={keyPressTitle}
          />
        </Container>
      </PopupBg>

      <PopupBg
        show={popupShow}
        onClick={() => { setPopupShow(false); }}
      >
        <Container
          onClick={(e) => e.stopPropagation()}
          css={css`
            flex-direction: column;
            justify-content: flex-start;
            align-items: flex-start;
            background: #C1FFD6;
            padding: 1rem;
            width: 50%;
          `}
        >
          <Container
            onClick={(e) => e.stopPropagation()}
            css={css`
            flex-direction: row;
            flex-wrap: nowrap;
            justify-content: space-between;
            align-items: flex-start;
            background: #C1FFD6;
            width: 100%;
          `}
          >
            <Title text={title} />
            <span
              className="material-icons-outlined click"
              onClick={() => { setPopupShow(false); }}
              onKeyPress={() => { setPopupShow(false); }}
              role="button"
              tabIndex={0}
            >
              close
            </span>
          </Container>

          <Text
            text={cardData}
          />

          <Container
            css={css`
              flex-direction: row;
              justify-content: space-between;
              width: 100%;
              padding: 0;
            `}
          >
            <Subtitle text="Description" />
            <Input
              placeholder="Type description here..."
              onChange={changeDesc}
              value={descHidden}
              onKeyPress={keyPressDesc}
              css={css`width: 100%;`}
              hidden={descEditHidden}
            />
            <Button text="Edit description" onClick={clickDesc} />
          </Container>
          <Container
            css={css`
              flex-direction: row;
              justify-content: space-between;
              width: 100%;
              align-items: flex-start;
              flex-wrap: nowrap;
            `}
          >
            <Text
              size="18px"
              text={desc}
            />
            <Clickable
              onClick={deleteDesc}
            >
              <span
                className="material-icons-outlined"
              >
                close
              </span>
            </Clickable>
          </Container>
          <Comments
            cardName={cardName}
            cardId={cardId}
            setComm={setComm}
          />
        </Container>
      </PopupBg>
    </>
  );
};
