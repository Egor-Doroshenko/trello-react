import React, { useState } from 'react';
import { css } from 'styled-components';
import { Container } from '../../ui/Container';
import { Input } from '../../ui/Input';
import { Button } from '../../ui/Button';

interface InputFormProps {
  onAdd?(cardName: string): void
}

export const InputForm: React.FunctionComponent<InputFormProps> = (props) => {
  const [cardName, setCardName] = useState<string>('');

  const changeHandler = (event: React.ChangeEvent<HTMLInputElement>):void => {
    setCardName(event.target.value);
  };

  const keyPressHandler = (event: React.KeyboardEvent):void => {
    if (event.key === 'Enter') {
      if (cardName !== '') {
        props.onAdd?.(cardName);
        setCardName('');
      }
    }
  };

  const clickHandler = ():void => {
    if (cardName !== '') {
      props.onAdd?.(cardName);
      setCardName('');
    }
  };

  return (
    <Container
      css={css`
        flex-wrap: nowrap;
        padding: 0;
        width: 100%;
        align-items: flex-start;
      `}
    >
      <Input
        onChange={changeHandler}
        onKeyPress={keyPressHandler}
        value={cardName}
        placeholder="Type here..."
        borderRadiusSides="left"
      />
      <Button
        onClick={clickHandler}
        borderRadiusSides="right"
        text="Add"
        css={css`
          height: 50px;
          margin-bottom: 0;
        `}
      />
    </Container>
  );
};
