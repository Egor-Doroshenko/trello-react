import React, { useState } from 'react';
import { css } from 'styled-components';
import { commsNum } from '../../utils/commsNum';
import { Button } from '../../ui/Button';
import { Container } from '../../ui/Container';
import { Text } from '../../ui/Text';
import { Popup } from '../Popup/index';

interface CardProps {
  cardName: string,
  colName: string,
  onRemove: (id: number, name: string) => void,
  id: number,
  author: string
}

export const Card:
React.FunctionComponent<CardProps> = ({
  cardName,
  colName,
  onRemove,
  id,
  author,
}: CardProps) => {
  const [comm, setComm] = useState(commsNum(cardName, id));
  const commsNumText = `Comments: ${comm}`;

  return (
    <Container
      css={css`
        flex-direction: column;
        align-items: flex-start;
        justify-content: flex-start;
        background: #84FFAD;
        padding: 0.5rem 0.5rem 0;
        width: 100%;
      `}
    >
      <Popup
        cardName={cardName}
        colName={colName}
        setComm={setComm}
        author={author}
        cardId={id}
      />
      <Text size="16px" text={commsNumText} />
      <Button
        text="Delete card"
        danger
        onClick={
        () => onRemove(id, cardName)
}
      />
    </Container>
  );
};
