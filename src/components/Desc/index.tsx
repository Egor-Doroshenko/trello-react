import React from 'react';
import { css } from 'styled-components';
import { Container } from '../../ui/Container';
import { Column } from '../Column/index';

const Desc: React.FunctionComponent = () => {
  let colsListStr = localStorage.getItem('colsList')!;
  if (!colsListStr) {
    localStorage.setItem(
      'colsList', JSON.stringify(['TODO', 'In progress', 'Testing', 'Done']),
    );
    colsListStr = localStorage.getItem('colsList')!;
  }
  const colsList = JSON.parse(colsListStr) as string[];

  return (
    <Container
      css={css`
        max-width: 100vw;
        background: #81e3a2;
        flex-direction: row;
        justify-content: space-evenly;
        align-items: flex-start;
        padding: 1rem;
      `}
    >
      <Column colName={colsList[0]} />
      <Column colName={colsList[1]} />
      <Column colName={colsList[2]} />
      <Column colName={colsList[3]} />
    </Container>
  );
};

export default Desc;
