import React from 'react';
import { css } from 'styled-components';
import { Container } from '../../ui/Container';
import { ICard } from '../../interfaces';
import { Card } from '../Card';
import { Text } from '../../ui/Text';

type CardListProps = {
  cards: ICard[],
  onRemove: (id: number, name: string) => void,
  colName: string,
}

export const CardList:
React.FunctionComponent<CardListProps> = ({
  cards,
  onRemove,
  colName,
}: CardListProps) => {
  if (cards.length === 0) {
    return (
      <Text size="18px" text="Add your first card!" />
    );
  }
  return (
    <Container
      css={css`
        padding: 1rem 0;
        width: 100%;
      `}
    >
      {cards.map((card) => (
        <Card
          key={card.id}
          cardName={card.name}
          colName={colName}
          onRemove={onRemove}
          id={card.id}
          author={card.author}
        />
      ))}
    </Container>
  );
};
